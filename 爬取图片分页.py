# -*- coding: utf-8 -*-
# !/usr/bin/env python
# !/usr/bin/python3
# @Time    : 2021/3/16 12:45
# @Author  : buke-freedom
# @File    : 爬取图片分页.py
"""
# @description：

"""
import requests
import re
import os

if __name__ == "__main__":
    # 创建一个文件夹，保存所有的图片
    if not os.path.exists('./qiutupics'):
        os.mkdir('./qiutupics')
        print('文件夹创建成功！！！')
    # 爬取图片
    # url = "https://pic.qiushibaike.com/system/pictures/12414/124149280/medium/ODXGZV7KWGTU8COG.jpg"
    # content返回的是二进制数据的图片数据
    # text（字符串） content（二进制） json（对象）
    # img_data = requests.get(url=url).content
    # with open('./qiutu.jpg','wb') as fp:
    # fp.write(img_data)
    # UA伪装：将对应的User_Agent封装到一个字典中
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:86.0) Gecko/20100101 Firefox/86.0"
    }
    # 设置一个通用的url模块
    url = 'https://www.qiushibaike.com/imgrank/page/%d/'
    # pageNum = 1
    for pageNum in range(1, 14):
        # 对应页码的url
        new_url = format(url % pageNum)

        # 使用通用爬虫对url对应的一整张页面进行爬取
        page_text = requests.get(url=new_url, headers=headers).text
        '''
        图片标签内容：
         <div class="thumb"> 
        <a href="/article/124138928" target="_blank">
        <img src="//pic.qiushibaike.com/system/pictures/12413/124138928/medium/SMAL0TFVGCB7Y34E.jpg" alt="糗事#124138928" class="illustration" width="100%" height="auto">
        </a>
        </div>
        对应正则表达式：
        ex = '<div class="thumb">.*?<img src="(.*?)" alt=.*?</div>'
        '''
        # 使用聚焦爬虫将页面中的所有图片进行解析、提取
        # ex = '<div class ="thumb">.*?<img src="(.*?)" alt.*?< / div >'
        ex = '<div class="thumb">.*?<img src="(.*?)" alt=.*?</div>'
        img_src_list = re.findall(ex, page_text, re.S)
        print(img_src_list)
        for src in img_src_list:
            # 拼接处一个完整的图片url
            src = 'https:' + src
            # 请求到了图片的二进制数据
            img_data = requests.get(url=src, headers=headers).content
            # 生成图片名称
            img_name = src.split('/')[-1]
            # 图片最终版存储的路径
            img_path = './qiutupics/' + img_name
            with open(img_path, 'wb') as fp:
                fp.write(img_data)
                print(img_name, '下载成功！！')
    print("爬取结束！！！")
