# coding=gbk
"""
作者：buke-freedom
时间：2021-3-7
摘要：

args将参数存到元组中，kwargs将参数存到字典中

"""


def stu_register(name, age, *args, **kwargs):
    print(name, age, args, kwargs)
    print(kwargs.get("addr"))


stu_register("doublechuan", 22, "M", addr="zhengzhou")

