"""
作者：buke-freedom
时间：2021-2-2
摘要：本次学习img2pdf

将指定目录里面的所有.jpg照片转换成.pdf（一个照片转换成一个pdf，pdf文件名字和照片名字一致）

"""
import os
import img2pdf


def from_photo_to_pdf(photo_path):
    photo_list = []  # 建立一个存放目录中所有照片路径的列表
    for fname in os.listdir(photo_path):  # 将指定目录中的所有文件名字进行遍历
        if not fname.endswith(".jpg"):  # 如果不是jpg文件就舍弃
            continue
        path = os.path.join(photo_path, fname)  # 将指定目录和jpg文件名字进行组合
        if os.path.isdir(path):  # 如果不是路径则舍弃
            continue
        photo_list.append(path)  # 将得到的文件目录存放到列表中
    # print(photo_list)
    # 1、指定pdf的单页的宽和高
    # A4纸张
    a4inpt = (img2pdf.mm_to_pt(210), img2pdf.mm_to_pt(297))
    layout_fun = img2pdf.get_layout_fun(a4inpt)
    j = 0
    # print(len(photo_list))
    print(photo_list[0].replace('.jpg', '.pdf'))
    while j < len(photo_list):
        # with open(photo_path+'\\1result.pdf', 'wb') as f:
        with open(photo_list[j].replace('.jpg', '.pdf'), 'wb') as f:
            f.write(img2pdf.convert(photo_list[j], layout_fun=layout_fun))
        j += 1


if __name__ == '__main__':
    photo_path = r"C:\Users\doubl\Desktop\picture"
    from_photo_to_pdf(photo_path)
