# -*- coding: utf-8 -*-
# !/usr/bin/env python
# !/usr/bin/python3
# @Time    : 2021/3/15 19:30
# @Author  : buke-freedom
# @File    : 爬取豆瓣电影.py
"""
# @description：

"""
import requests
import json

if __name__ == "__main__":
    url = "https://movie.douban.com/j/chart/top_list"
    param = {
        "type": "5",
        "interval_id": "100:90",
        "action": "",
        "start": "0",  # 从库中的第几部去取
        "limit": "20",  # 一次取出的个数
    }
    # UA伪装：将对应的User_Agent封装到一个字典中
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:86.0) Gecko/20100101 Firefox/86.0"
    }
    response = requests.get(url=url, params=param, headers=headers)

    list_data = response.json()

    fp = open("./douban.json", "w", encoding="utf-8")
    json.dump(list_data, fp=fp, ensure_ascii=False)

    print("抓取结束！")
