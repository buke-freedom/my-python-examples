# -*- coding: utf-8 -*-
# !/usr/bin/env python
# !/usr/bin/python3
# @Time    : 2021/3/7 11:59
# @Author  : buke-freedom
# @File    : 内置函数.py
# @description：

a = -10

print(abs(a))
print(bool(0))
print(bool(1))
# all()全部为真
li = [1, 23, 4, 5, 65, 6, 0]
print(all(li))
# any()任一值为true
print(any(li))

print(chr(97))  # 打印值对应的ASCII
print(ord("a"))  # 打印ASCII对应的值

# dict
print(dict())
print(dict(name="doublechuan", age=28))

print(dir())  # 打印当前文件的所有变量

print(locals())  # 打印当前文件的所有变量和变量值

l = list(range(10))
print(l)


def calc(a):
    return a ** 2


m = map(calc, l)  # 并没有执行，只有在循环的时候才调用
for i in m:
    print(i)

print(max(111111, 222222222222))
print(max(l))
print(min(l))
print(sum(l))

print(round(3.1415926, 2))
print(str(12345679))
