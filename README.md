# My Python Examples

#### 介绍
python学习程序例子集合

#### 软件架构
学习python基础知识
#### pycharm预定义文件模板变量
    $ {PROJECT_NAME} - 当前项目的名称。
    $ {NAME} - 在文件创建过程中在“新建文件”对话框中指定的新文件的名称。
    $ {USER} - 当前用户的登录名。
    $ {DATE} - 当前的系统日期。
    $ {TIME} - 当前系统时间。
    $ {YEAR} - 今年。
    $ {MONTH} - 当月。
    $ {DAY} - 当月的当天。
    $ {HOUR} - 目前的小时。
    $ {MINUTE} - 当前分钟。
    $ {PRODUCT_NAME} - 将在其中创建文件的IDE的名称。
    $ {MONTH_NAME_SHORT} - 月份名称的前3个字母。 示例：1月，2月等
    $ {MONTH_NAME_FULL} - 一个月的全名。 示例：1月，2月等

#### 函数
将重复的代码提出来，放到一个公共区域，谁想用，直接调用就行
    
    
    def function_name(parameter1，parameter2):
        函数语句
    
默认参数:

当实参不填的时候就为默认参数

    def function_name(parameter1，parameter2="默认值"):
    
##### 关键参数（指定参数）

##### 非固定参数

    def stu_register(name,age,*args,**kwargs):
        print(name,age,args)
        args将参数存到元组中，kwargs将参数存到字典中
        
##### 内置函数
    abs() 去绝对值
    all() 
    bool() 判断真假

#### 模块
##### 内置标准模块
300多个

##### 第三方开源模块

#####自定义模块

#### OS模块
os.getcwd()当前python脚本工作的目录路径

os.listdir()获取当前目录下所有的文件

os.remove()删除一个文件

os.removedirs()删除多个目录

os.path.isfile()检验给出的路径是否是一个文件

os.path.isdir()检验给出的路径是否是一个目录

os.path.exists()检测路径是否存在

os.path.dirname()获取路径名

os.path.abspath()获取文件的绝对路径

os.path.basename()获取文件名

os.system()运行shell命令

os.rename(old, new)重命名

os.makedirs(r"C:\te\w")创建多级目录

os.mkdir("test")创建单级目录

os.stat(file)获取文件属性

os.path.getsize(filename)获取文件大小




