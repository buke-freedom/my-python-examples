# -*- coding: utf-8 -*-
# !/usr/bin/env python
# !/usr/bin/python3
# @Time    : 2021/3/9 19:55
# @Author  : buke-freedom
# @File    : datetime模块.py
"""
# @description：
datetime module learn
"""
import datetime

print(datetime.datetime.now())

print(datetime.datetime.fromtimestamp(24345366376))
d = datetime.datetime.now()

print(d + datetime.timedelta(5, hours=5))

print(d.replace(year=2022, month=10))
