"""
作者：buke-freedom
时间：2021-3-2
摘要：本次学习文件的操作

r是文件的读取模式

"""
f = open("file_list", mode="r")

print(f.readline())
print("-------------------------------")
print(f.read())

f.close()
