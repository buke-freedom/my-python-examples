# -*- coding: utf-8 -*-
# !/usr/bin/env python
# !/usr/bin/python3
# @Time    : 2021/4/24 17:32
# @Author  : buke-freedom
# @File    : QLabelBuddy.py
"""
# @description：

"""

from PyQt5.QtWidgets import QApplication, QLabel, QDialog, QGridLayout
from PyQt5.QtWidgets import QPushButton, QLineEdit
import sys


class QLabelBuddy(QDialog):
    def __init__(self):
        super().__init__()
        self.initUi()

    def initUi(self):
        self.setWindowTitle('QLabel与伙伴控件')
        name_label = QLabel('&Name', self)
        name_lineedit = QLineEdit(self)
        # 设置label控件和lineedit之间的伙伴关系
        name_label.setBuddy(name_lineedit)

        pwd_label = QLabel('&Password', self)
        pwd_lineedit = QLineEdit(self)
        # 设置label控件和lineedit之间的伙伴关系
        pwd_label.setBuddy(pwd_lineedit)

        btn_OK = QPushButton('&OK')
        btn_cancel = QPushButton('&Cancel')

        grid = QGridLayout()
        grid.addWidget(name_label, 0, 0)
        # layout.addWidget(pwd_label)
        grid.addWidget(name_lineedit, 0, 1, 1, 2)  # 位置在0行，1列，占用1行两列

        grid.addWidget(pwd_label, 1, 0)
        grid.addWidget(pwd_lineedit, 1, 1, 1, 2)  # 位置在1行，1列，占用1行两列

        grid.addWidget(btn_OK, 2, 0)
        grid.addWidget(btn_cancel, 2, 1)
        self.setLayout(grid)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    qlabel = QLabelBuddy()
    qlabel.show()
    sys.exit(app.exec())
    pass
