# -*- coding: utf-8 -*-
# !/usr/bin/env python
# !/usr/bin/python3
# @Time    : 2021/3/15 19:49
# @Author  : buke-freedom
# @File    : 爬取肯德基店铺信息.py
"""
# @description：

"""
import requests
import json

if __name__ == "__main__":
    url = "http://www.kfc.com.cn/kfccda/ashx/GetStoreList.ashx?op=keyword"

    keyword = input("请输入需要查询的城市名字：")

    param1 = {
        "cname": "",
        "pid": "",
        "keyword": keyword,
        "pageIndex": "1",
        "pageSize": "10",
    }
    param2 = {
        "cname": "",
        "pid": "",
        "keyword": keyword,
        "pageIndex": "2",
        "pageSize": "10",
    }
    # UA伪装：将对应的User_Agent封装到一个字典中
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:86.0) Gecko/20100101 Firefox/86.0"
    }
    response1 = requests.post(url=url, data=param1, headers=headers)
    response2 = requests.post(url=url, data=param2, headers=headers)

    web_text1 = response1.text
    web_text2 = response2.text
    fp = open("./kfc.json", "a", encoding="utf-8")
    json.dump(web_text1, fp=fp, ensure_ascii=False)
    json.dump(web_text2, fp=fp, ensure_ascii=False)

    print("爬取结束！！！！")
