# coding=gbk
"""
作者：buke-freedom
时间：2021-3-6
摘要：本次学习文件的修改

全局文本检索替换
在命令行中执行：
python file_replace.py 粗 细 file1
"""
import sys

# print(sys.argv)
# 从命令行中获取参数信息
old_str = sys.argv[1]
new_str = sys.argv[2]
file_name = sys.argv[3]
# 打开指定文件
f = open(file_name, "r+")
# 将指定文件内容写到内存中
data = f.read()
# 统计文件中需要更换的字符串的数量
old_str_count = data.count(old_str)
# 替换指定字符串
new_data = data.replace(old_str, new_str)

# 清空文件
f.seek(0)
f.truncate()

# 将新的内容写到文件中
f.write(new_data)
f.close()
print(f"成功替换'{old_str}'成'{new_str}'，共计'{old_str_count}'处")
