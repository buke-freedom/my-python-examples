# -*- coding: utf-8 -*-
# !/usr/bin/env python
# !/usr/bin/python3
# @Time    : 2021/3/9 19:40
# @Author  : buke-freedom
# @File    : 时间处理模块.py
"""
# @description：
time module learn
"""
import time

print(time.time())

# time.sleep(5)#延时5秒

print(time.localtime())
print(time.gmtime())

print(time.mktime(time.localtime()))

print(time.strftime("%Y-%m-%d %H:%M:%S"))  # 时间转字符串
time_str = time.strftime("%Y-%m-%d %H:%M:%S")
print(time.strptime(time_str, "%Y-%m-%d %H:%M:%S"))  # 字符串转时间
