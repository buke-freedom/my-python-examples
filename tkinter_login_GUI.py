# -*- coding: utf-8 -*-
# !/usr/bin/env python
# !/usr/bin/python3
# @Time    : 2021/3/18 20:46
# @Author  : buke-freedom
# @File    : tkinter_login_GUI.py
"""
# @description：

"""
from tkinter import Tk, messagebox, Toplevel
# 小写的tk代表的是组件
import tkinter as tk


# 读取用户文件里的数据
def read_data():
    """
    读取用户文件中的账号和密码数据，用于与用户输入信息进行比对
    :return:
    """
    with open('User_data.txt', 'r') as fp:
        rows = fp.readlines()
        user_data_dict = {

        }
        for row in rows:
            user_data = row.strip().split(':')
            # print(user_data)
            user_data_dict[user_data[0]] = user_data[1]
        return user_data_dict


# 登录按钮事件处理回调

def user_login():
    """
    当用户点击登录按钮时，调用此函数，用于判断用户输入账号和密码是否正确，并提示响应信息
    :return:
    """
    # print(user_name_text.get())
    # print(password_text.get())
    name = user_name_text.get()
    pwd = password_text.get()
    user_dict = read_data()
    if name != '' and pwd != '':
        if name in user_dict.keys():
            if pwd == user_dict[name]:
                messagebox.showinfo(title='成功', message='恭喜你登录成功！')
            else:
                messagebox.showerror(title='错误', message='密码错误！')
        else:
            messagebox.showerror(title='错误', message='用户不存在！')
    else:
        messagebox.showerror(title='错误', message='账户或密码为空！')


# 注册按钮事件处理回调
def user_register():
    """

    :return:
    """
    top = Toplevel()
    top.title('注册')
    top.geometry('250x200')
    # 账号、密码、确认密码、注册按钮
    # 布局方式 ——组件以何种方式组织
    #   -place通过x，y坐标方式进行定位(精确)
    #   -grid网格布局方式（有规律）
    # 账号文本框
    tk.Label(top, text='账号', font=("宋体", 14), width=8).grid(row=1, column=0)
    reg_user_name = tk.StringVar()
    tk.Entry(top, textvariable=reg_user_name, font=("宋体", 14), width=12).grid(row=1, column=1)
    # 密码文本框
    tk.Label(top, text='密码', font=("宋体", 14), width=8).grid(row=2, column=0)
    reg_pwd = tk.StringVar()
    tk.Entry(top, textvariable=reg_pwd, font=("宋体", 14), width=12).grid(row=2, column=1)
    # 确认密码文本框
    tk.Label(top, text='确认密码', font=("宋体", 14), width=8).grid(row=3, column=0)
    reg_confirm_pwd = tk.StringVar()
    tk.Entry(top, textvariable=reg_confirm_pwd, font=("宋体", 14), width=12).grid(row=3, column=1)

    # 注册按钮
    def execute_register():
        """
        当按下注册按钮执行注册程序，主要是将用户输入的密码进行比对是否两次一致，如果一致则将账号和密码写入用户文件
        :return:
        """
        name = reg_user_name.get()  # 获取账号文本框中内容
        pwd = reg_pwd.get()  # 获取密码文本框中内容
        confirm_pwd = reg_confirm_pwd.get()  # 获取确认密码文本框内容
        if pwd == confirm_pwd:  # 判断两次密码是否一致
            with open('User_data.txt', 'a') as fp:  # 以追加模式打开用户文件
                fp.writelines(name + ":" + pwd + '\n')  # 写入注册用户账号加密码，以\n结尾，确保用户文件中每个用户一行的格式
                fp.flush()
                fp.close()
                messagebox.showinfo(title='成功', message='注册成功！')
                # 窗口销毁
                top.destroy()
        else:
            messagebox.showerror(title='错误', message='密码不一致')

    # columnspan=2是合并两列，pady=15边距
    tk.Button(top, text='注册', font=("宋体", 14), width=10, command=execute_register).grid(row=4, columnspan=2, pady=15)


if __name__ == "__main__":
    my_window = Tk()
    my_window.title('Login')
    # 设置窗口大小并居中显示
    screen_width, screen_height = my_window.maxsize()
    width = 320
    height = 240
    align_str = '%dx%d+%d+%d' % (width, height, (screen_width - width) / 2, (screen_height - height) / 2)
    my_window.geometry(align_str)
    # 设置窗口是否可以缩放
    my_window.resizable(width=False, height=False)
    # 添加标签
    user_name_label = tk.Label(my_window, text='账号', font=("宋体", 14))
    user_name_label.place(x=30, y=30)
    password_label = tk.Label(my_window, text='密码', font=("宋体", 14))
    password_label.place(x=30, y=70)
    # 输入框
    user_name_text = tk.StringVar()
    user_name_text.set("输入账号")
    user_name_entry = tk.Entry(my_window, textvariable=user_name_text, font=("宋体", 14), width=20)
    user_name_entry.place(x=80, y=30)

    password_text = tk.StringVar()
    password_text.set("输入密码")
    password_entry = tk.Entry(my_window, textvariable=password_text, font=("宋体", 14), width=20)
    password_entry.place(x=80, y=70)

    # 按钮
    # 登录按钮，事件处理
    user_login_button = tk.Button(my_window, text='登录', font=("宋体", 14), command=user_login)
    user_login_button.place(x=50, y=120)
    # 注册按钮，事件处理
    user_reg_button = tk.Button(my_window, text='注册', font=("宋体", 14), command=user_register)
    user_reg_button.place(x=200, y=120)
    my_window.mainloop()
    # read_data()
