# -*- coding: utf-8 -*-
# !/usr/bin/env python
# !/usr/bin/python3
# @Time    : 2021/4/6 19:59
# @Author  : buke-freedom
# @File    : FirstMainWindow.py
"""
# @description：
-QMaindow:
-QApplication:
-QDesktopWidget:
-QHBoxLayout:
-QWidget:
-QPushButton:按钮控件

"""
import sys
from PyQt5.QtWidgets import QMainWindow, QApplication, QDesktopWidget, QHBoxLayout, QWidget, QPushButton
from PyQt5.QtGui import QIcon


class FirstMainWindow(QMainWindow):
    def __init__(self, parent=None):
        super(FirstMainWindow, self).__init__(parent)

        # set main windows title
        self.setWindowTitle('first window')
        # set main window size
        self.resize(400, 300)

        self.status = self.statusBar()
        self.status.showMessage('只存在5秒的消息', 5000)
        self.center()

        # 添加按钮
        self.button1 = QPushButton('退出程序')
        self.button1.clicked.connect(self.onclick_button)

        layout = QHBoxLayout()
        layout.addWidget(self.button1)

        mainFrame = QWidget()
        mainFrame.setLayout(layout)

        self.setCentralWidget(mainFrame)

    # 按钮的单击事件方法
    def onclick_button(self):
        sender = self.sender()
        print(sender.text() + '按钮被按下')
        app = QApplication.instance()
        app.quit()

    def center(self):
        """
        控制窗口显示在屏幕中心的方法
        :return:
        """
        # 获得屏幕中心点
        screen_center = QDesktopWidget().availableGeometry().center()
        # 获取窗口
        window = self.geometry()
        window.moveCenter(screen_center)
        self.move(window.topLeft())


if __name__ == "__main__":
    app = QApplication(sys.argv)
    app.setWindowIcon(QIcon('./a.ico'))
    main = FirstMainWindow()

    main.show()

    sys.exit(app.exec())
