# -*- coding: utf-8 -*-
# !/usr/bin/env python
# !/usr/bin/python3
# @Time    : 2021/5/4 20:54
# @Author  : buke-freedom
# @File    : QTextEditDemo.py
"""
# @description：

"""
from PyQt5.QtWidgets import QWidget, QApplication, QTextEdit, QPushButton, QVBoxLayout
import sys


class QTextEditDemo(QWidget):
    def __init__(self):
        super(QTextEditDemo,self).__init__()
        self.initUi()

    def initUi(self):
        self.setWindowTitle('QTextEdit演示')
        self.resize(300, 280)

        self.textedit = QTextEdit()
        self.buttontext = QPushButton('显示文本')
        self.buttonHTML = QPushButton('显示HTML')

        self.buttontotext = QPushButton('获取文本')
        self.buttontoHTML = QPushButton('获取HTML')

        layout = QVBoxLayout()
        layout.addWidget(self.textedit)
        layout.addWidget(self.buttontext)
        layout.addWidget(self.buttontotext)
        layout.addWidget(self.buttonHTML)
        layout.addWidget(self.buttontoHTML)

        self.setLayout(layout)

        self.buttontext.clicked.connect(self.onClick_Buttontext)
        self.buttontotext.clicked.connect(self.onClick_Buttontotext)
        self.buttonHTML.clicked.connect(self.onClick_ButtonHTML)
        self.buttontoHTML.clicked.connect(self.onClick_ButtontoHTML)

    def onClick_Buttontext(self):
        self.textedit.setPlainText('Hello World，世界你好')

    def onClick_Buttontotext(self):
        print(self.textedit.toPlainText())

    def onClick_ButtonHTML(self):
        self.textedit.setHtml('<font color="blue" size="5">Hello World </font>')

    def onClick_ButtontoHTML(self):
        print(self.textedit.toHtml())


if __name__ == "__main__":
    app = QApplication(sys.argv)
    main = QTextEditDemo()
    main.show()
    sys.exit(app.exec())
    pass
