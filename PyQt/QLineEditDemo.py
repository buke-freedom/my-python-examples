# -*- coding: utf-8 -*-
# !/usr/bin/env python
# !/usr/bin/python3
# @Time    : 2021/5/4 20:22
# @Author  : buke-freedom
# @File    : QLineEditDemo.py
"""
# @description：

"""
from PyQt5.QtWidgets import QWidget, QLineEdit, QFormLayout, QApplication
from PyQt5.QtGui import QIntValidator, QDoubleValidator, QRegExpValidator, QFont
# from PyQt5.QtGui import *
from PyQt5.QtCore import Qt
import sys


class QLineEditDemo(QWidget):
    def __init__(self):
        super(QLineEditDemo, self).__init__()
        self.initUi()

    def initUi(self):
        edit1 = QLineEdit()
        # use int validator使用int校验器
        edit1.setValidator(QIntValidator())
        edit1.setMaxLength(4)  # 不超过9999
        edit1.setAlignment(Qt.AlignRight)  # 文本为右对齐
        edit1.setFont(QFont('Arial', 20))

        edit2 = QLineEdit()
        edit2.setValidator(QDoubleValidator(0.99, 99.99, 2))

        edit3 = QLineEdit()
        edit3.setInputMask('99_9999_999999;#')

        edit4 = QLineEdit()
        edit4.textChanged.connect(self.textchanged)

        edit5 = QLineEdit()
        edit5.setEchoMode(QLineEdit.Password)
        edit5.editingFinished.connect(self.enterpress)

        edit6 = QLineEdit('Hello PyQt5')
        edit6.setReadOnly(True)

        formlayout = QFormLayout()
        formlayout.addRow('整数校验', edit1)
        formlayout.addRow('浮点数校验', edit2)
        formlayout.addRow('Input Mask', edit3)
        formlayout.addRow('文本变化', edit4)
        formlayout.addRow('密码', edit5)
        formlayout.addRow('只读', edit6)

        self.setLayout(formlayout)
        self.setWindowTitle('QLineEdit综合案例')

    def textchanged(self, text):
        print('输入的内容：' + text)

    def enterpress(self):
        print('已输入值')


if __name__ == "__main__":
    app = QApplication(sys.argv)
    main = QLineEditDemo()
    main.show()
    sys.exit(app.exec())
    pass
