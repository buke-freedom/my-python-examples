# -*- coding: utf-8 -*-
# !/usr/bin/env python
# !/usr/bin/python3
# @Time    : 2021/5/4 21:21
# @Author  : buke-freedom
# @File    : QPushButtonDemo.py
"""
# @description：
按钮控件（QPushButton）
所有的按钮控件的父类：QAbstractButton

按钮控件有：
QPushButton
AToolButton:工具条按钮
QRadioButton:单选按钮
QCheckBox:多选按钮
"""
from PyQt5.QtWidgets import QWidget, QApplication, QDialog, QPushButton, QVBoxLayout
from PyQt5.QtGui import QIcon, QPixmap
import sys


class QPushButtonDemo(QDialog):
    def __init__(self):
        super(QPushButtonDemo, self).__init__()
        self.initUI()

    def initUI(self):
        self.setWindowTitle('QPushButton Demo')
        layout = QVBoxLayout()

        self.button1 = QPushButton('第1个按钮')
        self.button1.setText('First Button1')
        self.button1.setCheckable(True)
        self.button1.toggle()  # 选中状态

        self.button1.clicked.connect(lambda: self.which_button(self.button1))
        self.button1.clicked.connect(self.button_state)
        layout.addWidget(self.button1)

        # 在文本前面显示图片
        self.button2 = QPushButton('图像按钮')
        self.button2.setIcon(QIcon(QPixmap('a.jpg')))
        self.button2.clicked.connect(lambda: self.which_button(self.button2))
        layout.addWidget(self.button2)

        self.button3 = QPushButton('不可用按钮')
        self.button3.setEnabled(False)
        layout.addWidget(self.button3)

        self.button4 = QPushButton('&MyButton')
        self.button4.setDefault(True)
        self.button4.clicked.connect(lambda: self.which_button(self.button4))
        layout.addWidget(self.button4)

        self.setLayout(layout)
        self.resize(300,200)

    def button_state(self):
        if self.button1.isChecked():
            print('按钮1被选中')
        else:
            print('按钮1未被选中')
    def which_button(self, btn):
        print('被点击的按钮是<' + btn.text() + '>')


if __name__ == "__main__":
    app = QApplication(sys.argv)
    main = QPushButtonDemo()
    main.show()
    sys.exit(app.exec())
