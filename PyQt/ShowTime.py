# -*- coding: utf-8 -*-
# !/usr/bin/env python
# !/usr/bin/python3
# @Time    : 2021/5/7 20:52
# @Author  : buke-freedom
# @File    : ShowTime.py
"""
# @description：
动态显示当前时间

QTimer
QThread

多线程：用于同时完成多个任务

"""

from PyQt5.QtWidgets import QWidget, QApplication, QDialog, QPushButton, QVBoxLayout, QHBoxLayout, QComboBox, QLabel, \
    QGridLayout
from PyQt5.QtGui import QIcon, QPixmap
from PyQt5.QtCore import QTimer, QDateTime
import sys


class ShowTime(QWidget):
    def __init__(self):
        super(ShowTime, self).__init__()
        self.initUI()

    def initUI(self):
        self.setWindowTitle('动态显示当前时间')

        self.label = QLabel('显示当前时间')
        self.startbutton = QPushButton('开始')
        self.endbutton = QPushButton('结束')

        layout = QGridLayout()

        self.timer = QTimer()
        self.timer.timeout.connect(self.showtime)

        layout.addWidget(self.label, 0, 0, 1, 2)
        layout.addWidget(self.startbutton, 1, 0)
        layout.addWidget(self.endbutton, 1, 1)

        self.startbutton.clicked.connect(self.starttimer)
        self.endbutton.clicked.connect(self.endtimer)

        self.setLayout(layout)

    def showtime(self):
        time = QDateTime.currentDateTime()
        time_display = time.toString('yyyy-mm-dd hh:mm:ss dddd')
        self.label.setText(time_display)

    def starttimer(self):
        self.timer.start(1000)
        self.startbutton.setEnabled(False)
        self.endbutton.setEnabled(True)

    def endtimer(self):
        self.timer.stop()
        self.startbutton.setEnabled(True)
        self.endbutton.setEnabled(False)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    main = ShowTime()
    main.show()
    sys.exit(app.exec())
