# -*- coding: utf-8 -*-
# !/usr/bin/env python
# !/usr/bin/python3
# @Time    : 2021/5/6 21:01
# @Author  : buke-freedom
# @File    : QRadioButtonDemo.py
"""
# @description：
QRadioButton单选按钮控件
"""
from PyQt5.QtWidgets import QWidget, QApplication, QDialog, QPushButton, QVBoxLayout, QHBoxLayout, QRadioButton
from PyQt5.QtGui import QIcon, QPixmap
import sys


class QRadioButtonDemo(QWidget):
    def __init__(self):
        super(QRadioButtonDemo, self).__init__()
        self.initUI()

    def initUI(self):
        self.setWindowTitle('QRadioButton')

        layout = QHBoxLayout()

        self.button1 = QRadioButton('单选按钮1')
        self.button1.setChecked(True)
        self.button1.toggled.connect(self.buttonsatate)
        layout.addWidget(self.button1)

        self.button2 = QRadioButton('单选按钮2')
        # self.button1.setChecked(True)
        self.button2.toggled.connect(self.buttonsatate)
        layout.addWidget(self.button2)

        self.setLayout(layout)

    def buttonsatate(self):
        radiobutton = self.sender()

        if radiobutton.isChecked():
            print(radiobutton.text() + '被选中')
        else:
            print(radiobutton.text() + '被取消选中状态')


if __name__ == "__main__":
    app = QApplication(sys.argv)
    main = QRadioButtonDemo()
    main.show()
    sys.exit(app.exec())
