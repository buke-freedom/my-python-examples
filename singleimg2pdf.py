"""
作者：buke-freedom
时间：2021-2-2
摘要：本次学习img2pdf

将指定目录里面的所有.jpg照片转换成.pdf（一个照片转换成一个pdf，pdf文件名字和照片名字一致）

"""
import os
import img2pdf


def from_photo_to_pdf(photo_path):
    # 1、生成地址列表
    photo_list = os.listdir(photo_path)  # os.listdir() 方法用于返回指定的文件夹包含的文件或文件夹的名字的列表
    print(photo_list)
    photo_list = [os.path.join(photo_path, i) for i in photo_list]
    print(photo_list)
    # 1、指定pdf的单页的宽和高
    # A4纸张
    a4inpt = (img2pdf.mm_to_pt(210), img2pdf.mm_to_pt(297))
    # 我的自定义：
    # a4inpt = (img2pdf.mm_to_pt(720), img2pdf.mm_to_pt(1080))
    layout_fun = img2pdf.get_layout_fun(a4inpt)
    j = 0
    print(len(photo_list))
    print(photo_list[0].replace('.jpg', '.pdf'))
    while j < len(photo_list):

        # with open(photo_path+'\\1result.pdf', 'wb') as f:
        with open(photo_list[j].replace('.jpg', '.pdf'), 'wb') as f:
            f.write(img2pdf.convert(photo_list[j], layout_fun=layout_fun))
        j += 1


if __name__ == '__main__':
    photo_path = r"C:\Users\doubl\Desktop\picture"
    from_photo_to_pdf(photo_path)
