# -*- coding: utf-8 -*-
# !/usr/bin/env python
# !/usr/bin/python3
# @Time    : 2021/3/14 20:33
# @Author  : buke-freedom
# @File    : 初识爬虫.py
"""
# @description：

requests模块
    -urllib模块：比较老
    -requests模块：python中原生的基于网络请求的模块，功能强大，使用简单便捷，效率极高
    作用：模拟浏览器发请求
    使用：
        -指定url
        -发起请求
        -获取响应数据
        -持久化存储数据
    环境安装：
        -pip install requests
    实战编码：
        -需求：爬取搜狗首页的页面数据
"""
import requests

if __name__ == "__main__":
    # appoint url
    url = "https://www.sogou.com/"
    # 发起请求，get方法会返回一个响应对象
    response = requests.get(url=url, )
    # 获取响应数据，text 返回的是字符串形势的响应数据
    page_text = response.text
    print(page_text)
    # 存储
    with open("./sogou.html", "w", encoding="utf-8") as fp:
        fp.write(page_text)
    print("爬取操作结束！！！")
