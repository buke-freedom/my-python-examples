# -*- coding: utf-8 -*-
# !/usr/bin/env python
# !/usr/bin/python3
# @Time    : 2021/4/24 19:14
# @Author  : buke-freedom
# @File    : QLineEditValidator.py
"""
# @description：
QIntValidator：校验整数类型
QDoubleValidator校验浮点数类型
QRegExpValidator:校验正则表达式
"""

from PyQt5.QtWidgets import QWidget, QApplication, QLineEdit, QFormLayout

from PyQt5.QtGui import QIntValidator, QDoubleValidator, QRegExpValidator

from PyQt5.QtCore import QRegExp  # 正则表达式
import sys


class QLineEditValidator(QWidget):
    def __init__(self):
        super().__init__()
        self.initUi()

    def initUi(self):
        self.setWindowTitle('QLineEdit校验器')

        form = QFormLayout()
        int_lineedit = QLineEdit()
        double_lineedit = QLineEdit()
        validator_lineedit = QLineEdit()

        form.addRow("整数类型", int_lineedit)
        form.addRow("浮点数类型", double_lineedit)
        form.addRow("数字和字母", validator_lineedit)

        int_lineedit.setPlaceholderText("整数类型")
        double_lineedit.setPlaceholderText("浮点数类型")
        validator_lineedit.setPlaceholderText("数字和字母")

        # 整数校验器
        int_validator = QIntValidator(self)
        int_validator.setRange(1,99)
        # 浮点校验器 范围-360——360，精度：小数点两位
        double_validator = QDoubleValidator(self)
        double_validator.setRange(-360,360)
       #  double_lineedit.setNotation(QDoubleValidator.StandardNotation)
        # s设置精度 ，小数点后面几位
        # double_lineedit.setDecimals(2)

        # 数字和字母
        reg = QRegExp('[a-zA-Z0-9]+$')
        validator = QRegExpValidator(self)
        validator.setRegExp(reg)

        # 设置校验器
        int_lineedit.setValidator(int_validator)
        double_lineedit.setValidator(double_validator)
        validator_lineedit.setValidator(validator)

        self.setLayout(form)



if __name__ == "__main__":
    app = QApplication(sys.argv)
    main = QLineEditValidator()
    main.show()
    sys.exit(app.exec())
    pass
