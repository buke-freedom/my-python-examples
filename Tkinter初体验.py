# -*- coding: utf-8 -*-
# !/usr/bin/env python
# !/usr/bin/python3
# @Time    : 2021/3/18 20:31
# @Author  : buke-freedom
# @File    : Tkinter初体验.py
"""
# @description：

"""
from tkinter import Tk



if __name__ == "__main__":
    # 显示一个窗口
    my_window = Tk()
    # 标题
    my_window.title('我的窗口')
    # 设置窗口居中
    # 获取屏幕大小
    screen_width, screen_height = my_window.maxsize()
    # 设置窗口大小
    width = 240
    height = 480
    # %dx%d+%d+%d 宽x高+x+y（xy窗口坐标）
    align_str = '%dx%d+%d+%d' % (width, height, (screen_width - width) / 2, (screen_height - height) / 2)
    my_window.geometry(align_str)
    # 设置窗口是否可以缩放
    my_window.resizable(width=False, height=False)

    my_window.mainloop()
