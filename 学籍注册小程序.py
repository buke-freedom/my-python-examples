# -*- coding: utf-8 -*-
# !/usr/bin/env python
# !/usr/bin/python3
# @Time    : 2021/3/8 19:17
# @Author  : buke-freedom
# @File    : 学籍注册小程序.py
"""
# @description：
1.数据存到文件的格式
姓名，年龄，手机，身份证，学科
2.要确保手机和身份证，在文件里同样的数据只有一条
3.学科选择预设值好，让用手选择

seq = ['one', 'two', 'three']
    for i, element in enumerate(seq):
    print i, element
...
0 one
1 two
2 three

"""

user_db_file = "user_information.db"


def register_api():
    """
    用户注册函数，用于让用户输入姓名、年龄、手机号码、身份证、及选择的课程
    :return:
    """
    user_data = {}
    print("欢迎来到buke学习网站".center(50, "-"))
    print("请完成学籍注册！")

    name = input("姓名：").strip()
    age = input("年龄：").strip()

    phone = input("手机号码：").strip()
    if phone in phone_list:
        exit("该手机已注册！")
    user_id = input("身份证号：").strip()
    if user_id in user_id_list:
        exit("该身份证已注册！")

    course_list = ["python", "Java", "C", "C#", "PHP"]
    # enumerate() 函数用于将一个可遍历的数据对象(如列表、元组或字符串)组合为一个索引序列，同时列出数据和数据下标，一般用在 for 循环当中
    for index, course in enumerate(course_list):
        print(f"{index}.{course}")

    select_course = input("请选择想要学习的课程：")
    if select_course.isdigit():  # 判断输入的是否是数字
        select_course = int(select_course)
        # if select_course >= 0 and select_course <= len(course_list):
        if 0 <= select_course <= len(course_list):
            picked_course = course_list[select_course]
        else:
            exit("不合法的选项。。。")
    else:
        exit("不合法选项。。。")

    user_data["name"] = name
    user_data["age"] = age
    user_data["phone"] = phone
    user_data["user_id"] = user_id
    user_data["course"] = picked_course

    return user_data


def commit_to_db(filename, user_data):
    """
    把用户数据存到文件中
    :param filename:用户数据文件
    :param user_data:单个用户数据的dict
    :return:
    """
    f = open(filename, "a")
    row = f"{user_data['name']},{user_data['age']},{user_data['phone']},{user_data['user_id']},{user_data['course']}\n"
    f.write(row)
    f.close()


def check_data(filename):
    """
    校验用户输入的手机号和身份证号是否已存在在数据文件中
    :param filename: 用户数据文件
    :return:
    """
    f = open(filename)
    phone_list = []
    user_id_list = []
    for line in f:
        line = line.strip(",")
        phone = line[2]
        user_id = line[3]
        phone_list.append(phone)
        user_id_list.append(user_id)

    return phone_list, user_id_list


# 首先将用户数据文件中的手机号码和身份证号码提取出来，用于比较用户输入的号码是否已经存在
phone_list, user_id_list = check_data(user_db_file)
# 让用户注册个人信息
user_information = register_api()

print(user_information)
# 将新用户输入的信息存放到用户数据文件
commit_to_db(user_db_file, user_information)
