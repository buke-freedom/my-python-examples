# -*- coding: utf-8 -*-
# !/usr/bin/env python
# !/usr/bin/python3
# @Time    : 2021/4/6 21:01
# @Author  : buke-freedom
# @File    : pyqt5test1.py
"""
# @description：

"""

import sys

# QMainWindow 类提供了一个主要的应用程序窗口。
# 你用它可以让应用程序添加状态栏,工具栏和菜单栏。
from PyQt5.QtWidgets import QWidget, QMainWindow

from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QDesktopWidget, QMessageBox, QLabel, QPushButton, QAction
from PyQt5.QtWidgets import QAction, qApp
from PyQt5.QtWidgets import QLineEdit, QTextEdit
from PyQt5.QtWidgets import QHBoxLayout  # horizontal 水平布局
from PyQt5.QtWidgets import QVBoxLayout  # vertical   垂直布局
from PyQt5.QtWidgets import QGridLayout  # Grid       网格布局

from PyQt5.QtGui import QIcon


# 如果不想一个一个 导入，可以 import *
# from PyQt5.QtWidgets import *


class MyForm1(QWidget):
    def __init__(self):
        super(MyForm1, self).__init__()
        #self._init_ui_1()
        #self._init_ui_2()
        #self._init_ui_3()
        # self._init_ui_4()
        self._init_ui_5()
        pass

    def center(self):
        """
            控制窗口显示在屏幕中心的方法
        :return:
        """
        # 获得窗口
        qr = self.frameGeometry()
        # 获得屏幕中心点
        cp = QDesktopWidget().availableGeometry().center()
        # 显示到屏幕中心
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def _init_ui_1(self):
        """
            重置大小，中心显示
        :return:
        """
        self.resize(1000, 600)
        self.center()
        self.show()

    def _init_ui_2(self):
        """
            使用 绝对位置 布局元素位置
        :return:
        """
        lbl1 = QLabel('Zetcode', self)
        lbl1.move(15, 10)

        lbl2 = QLabel('tutorials', self)
        lbl2.move(35, 40)

        lbl3 = QLabel('for programmers', self)
        lbl3.move(55, 70)

        self.setGeometry(300, 300, 250, 150)
        self.setWindowTitle('Absolute')
        self.show()

    def _init_ui_3(self):
        """
            使用布局 来 布局 元素位置
        :return:
        """
        btn_ok = QPushButton("OK")
        btn_cancel = QPushButton("Cancel")

        hbox = QHBoxLayout()
        hbox.addStretch(1)
        hbox.addWidget(btn_ok)
        hbox.addWidget(btn_cancel)

        vbox = QVBoxLayout()
        vbox.addStretch(1)
        vbox.addLayout(hbox)

        self.setLayout(vbox)

        self.setGeometry(300, 300, 300, 150)
        self.setWindowTitle('Buttons')
        self.show()

    def _init_ui_4(self):
        """
            使用 网格布局 来 布局 元素位置
        :return:
        """
        grid = QGridLayout()
        self.setLayout(grid)

        names = [
            'Cls', 'Bck', '', 'Close',
            '7', '8', '9', '/',
            '4', '5', '6', '*',
            '1', '2', '3', '-',
            '0', '.', '=', '+'
        ]

        positions = [(i, j) for i in range(5) for j in range(4)]

        for position, name in zip(positions, names):

            if name == '':
                continue
            button = QPushButton(name)
            grid.addWidget(button, *position)

        self.move(300, 150)
        self.setWindowTitle('Calculator')
        self.show()

    def _init_ui_5(self):
        """
            网格布局 跨越 多行 或者 多列
        :return:
        """
        title = QLabel('Title')
        author = QLabel('Author')
        review = QLabel('Review')

        titleEdit = QLineEdit()
        authorEdit = QLineEdit()
        reviewEdit = QTextEdit()

        grid = QGridLayout()
        grid.setSpacing(10)

        grid.addWidget(title, 1, 0)
        grid.addWidget(titleEdit, 1, 1)

        grid.addWidget(author, 2, 0)
        grid.addWidget(authorEdit, 2, 1)

        grid.addWidget(review, 3, 0)
        grid.addWidget(reviewEdit, 3, 1, 5, 1)

        self.setLayout(grid)

        self.setGeometry(300, 300, 350, 300)
        self.setWindowTitle('Review')
        self.show()

    def closeEvent(self, event):
        """
            重写关闭窗口事件
        :param event:
        :return:
        """
        reply = QMessageBox.question(
            self, 'Message', 'Are you sure close window ?',
            QMessageBox.Yes | QMessageBox.No, QMessageBox.No
        )
        if reply == QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()
        pass


class MyForm2(QMainWindow):
    """
        QMainWindow 类提供了一个主要的应用程序窗口。
        你用它可以让应用程序添加状态栏,工具栏和菜单栏。
    """

    def __init__(self):
        super(MyForm2, self).__init__()
        self._init_ui_6()
        #self._init_ui_7()
        #self._init_ui_8()
        #self._init_ui_9()

    def _init_ui_6(self):
        """
            状态栏
        :return:
        """
        self.statusBar().showMessage('Ready')
        self.setGeometry(800, 300, 250, 150)
        self.setWindowTitle('Statusbar')
        self.show()

    def _init_ui_7(self):
        """
            菜单栏
        :return:
        """
        # QAction可以操作菜单栏,工具栏,或自定义键盘快捷键。

        # 创建一个事件和一个特定的图标和一个“退出”的标签。
        exitAction = QAction(QIcon('exit.png'), '&Exit', self)
        exitAction.setShortcut('Ctrl+Q')  # 定义该操作的快捷键。

        # 创建一个鼠标指针悬停在该菜单项上时的提示。
        exitAction.setStatusTip('Exit application')

        # # 第三行创建一个鼠标指针悬停在该菜单项上时的提示。
        exitAction.triggered.connect(qApp.quit)

        self.statusBar()

        # 创建一个菜单栏
        menubar = self.menuBar()
        # 添加菜单
        fileMenu = menubar.addMenu('&File')
        # 添加事件
        fileMenu.addAction(exitAction)

        self.setGeometry(800, 300, 300, 200)
        self.setWindowTitle('Menubar')
        self.show()

    def _init_ui_8(self):
        """
            工具栏
        :return:
        """
        exitAction = QAction(QIcon('exit24.png'), 'Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.triggered.connect(qApp.quit)

        self.toolbar = self.addToolBar('Exit')
        self.toolbar.addAction(exitAction)

        self.setGeometry(800, 300, 300, 200)
        self.setWindowTitle('Toolbar')
        self.show()

    def _init_ui_9(self):
        # 创建一个菜单条,工具栏和状态栏的小窗口
        textEdit = QTextEdit()
        self.setCentralWidget(textEdit)

        exitAction = QAction(QIcon('exit24.png'), 'Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setStatusTip('Exit application')
        exitAction.triggered.connect(self.close)

        self.statusBar()

        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&File')
        fileMenu.addAction(exitAction)

        toolbar = self.addToolBar('Exit')
        toolbar.addAction(exitAction)

        self.setGeometry(800, 300, 350, 250)
        self.setWindowTitle('Main window')
        self.show()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    form_1 = MyForm1()
    form_2 = MyForm2()
    sys.exit(app.exec_())
    pass
