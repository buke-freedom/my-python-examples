# -*- coding: utf-8 -*-
# !/usr/bin/env python
# !/usr/bin/python3
# @Time    : 2021/4/18 15:59
# @Author  : buke-freedom
# @File    : xpath.py
"""
# @description：

"""
import requests
from lxml import etree
if __name__ == "__main__":

    # UA伪装：将对应的User_Agent封装到一个字典中
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:86.0) Gecko/20100101 Firefox/86.0"
    }
    url = "https://zz.58.com/ershoufang/"
    page_text = requests.get(url=url,headers=headers).text

    #数据解析
    tree = etree.HTML(page_text)
    div_list = tree.xpath('//section[@class="list"]/div')
    for div in div_list:
        title = div.xpath('./a/div[2]/div/div/h3/@title')[0]
        print(title)