# coding=gbk
"""
作者：buke-freedom
时间：2021-3-6
摘要：本次学习文件的修改

1.先把文件内容加载到内存中
2.然后在内存中对文件进行修改
3.再将修改后的内容写进文件中

"""
f = open("file1", "r+")

# 加载到内存
file_data = f.read()
new_file_data = file_data.replace("思源黑体", "仿宋体仿")

# 清空文件
f.seek(0)  # 将光标移动到0位置
f.truncate()  # 从光标位置截断文件

# 将新内容写入文件中
f.write(new_file_data)
