# -*- coding: utf-8 -*-
# !/usr/bin/env python
# !/usr/bin/python3
# @Time    : 2021/4/8 20:25
# @Author  : buke-freedom
# @File    : 布局3.py
"""
# @description：

"""
import sys

# QMainWindow 类提供了一个主要的应用程序窗口。
# 你用它可以让应用程序添加状态栏,工具栏和菜单栏。
from PyQt5.QtWidgets import QWidget

from PyQt5.QtWidgets import (
    QApplication, QDesktopWidget, QHBoxLayout, QVBoxLayout,
    QPushButton, QListView,
)


class LayoutDemoByQWidget(QWidget):

    def __init__(self):
        super(LayoutDemoByQWidget, self).__init__()
        self.resize(1000, 600)  # 重置大小
        self._center_display()  # 中心显示
        self.vertical_layout()
        self.show()

    def _center_display(self):
        """
            控制窗口显示在屏幕中心的方法
        :return:
        """
        # 获得窗口
        qr = self.frameGeometry()
        # 获得屏幕中心点
        cp = QDesktopWidget().availableGeometry().center()
        # 显示到屏幕中心
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def vertical_layout(self):
        """
            垂直布局
        :return:
        """
        h_box_1 = QHBoxLayout()
        h_box_2 = QHBoxLayout()
        h_box_3 = QHBoxLayout()

        btn_ok = QPushButton('Ok', self)
        btn_cancel = QPushButton('Cancel', self)
        btn_exit = QPushButton('Exit', self)
        btn_ok.resize(10, 10)
        btn_cancel.resize(10, 10)
        btn_exit.resize(40, 10)

        # h_box_1.addStretch(1)
        h_box_1.addWidget(btn_ok)
        h_box_1.addWidget(btn_cancel)
        h_box_1.addWidget(btn_exit)

        list_view = QListView(self)
        h_box_2.addWidget(list_view)

        btn_1 = QPushButton('btn_1', self)
        btn_2 = QPushButton('btn_2', self)
        btn_3 = QPushButton('btn_3', self)
        h_box_3.addStretch(1)
        h_box_3.addWidget(btn_1)
        #h_box_3.addStretch(0)
        h_box_3.addWidget(btn_2)
        #h_box_3.addStretch(0)
        h_box_3.addWidget(btn_3)
        #h_box_3.addStretch(0)

        v_box = QVBoxLayout()
        # v_box.addStretch(1)
        v_box.addLayout(h_box_1)
        v_box.addLayout(h_box_2)
        v_box.addLayout(h_box_3)

        # 把 默认布局 设置成 v_box
        self.setLayout(v_box)
        pass


if __name__ == '__main__':
    app = QApplication(sys.argv)
    form = LayoutDemoByQWidget()
    sys.exit(app.exec_())
    pass