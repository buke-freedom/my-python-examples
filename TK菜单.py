# -*- coding: utf-8 -*-
# !/usr/bin/env python
# !/usr/bin/python3
# @Time    : 2021/3/21 10:55
# @Author  : buke-freedom
# @File    : TK菜单.py
"""
# @description：
tkinter菜单的创建
"""
from tkinter import Menu, Tk, Toplevel, Label, Entry, Button, Listbox, END
from tkinter import StringVar, Radiobutton, Checkbutton, IntVar
from tkinter.ttk import Combobox
from openpyxl import *


def write_excel(stu: list):
    """
    将新增的学生信息写入excel文件中
    :return:
    """
    work_book = load_workbook('stu.xlsx')  # 加载一个excel文件
    sheet = work_book.active  # 激活
    cur_row = sheet.max_row  # 获得目前的最大行
    new_row = cur_row + 1  # 计算要写入的行
    for data_cal in range(1, 7):  # 按行写入需要写入的数据
        sheet.cell(row=new_row, column=data_cal, value=str(stu[data_cal - 1]))  # cell按照行和列写入数据

    work_book.save(filename='stu.xlsx')  # 保存文件


# 读取excel数据 -> list返回类型
def read_excel() -> list:
    # 构建excel对象
    work_book = load_workbook('stu.xlsx')
    sheet = work_book.active
    stu_list = []
    # 读物excel中的数据
    for row in sheet.iter_rows(min_col=1, max_col=6, min_row=2, max_row=sheet.max_row):
        stu_temp = []
        for c in row:
            stu_temp.append(c.value)
            # print(c.value)
        stu_list.append(stu_temp)
    return stu_list


def set_center(width, height, screen_width, screen_height):
    align_str = '%dx%d+%d+%d' % (width, height, (screen_width - width) / 2, (screen_height - height) / 2)
    return align_str


def pop_add_stu_win():
    """
    添加学生信息
    :return:
    """
    stu_add_win = Toplevel()
    stu_add_win.title('添加学生')
    width = 250
    height = 300
    screen_width, screen_height = stu_add_win.maxsize()
    stu_add_win.geometry(set_center(width, height, screen_width, screen_height))

    # 姓名
    Label(stu_add_win, text='姓名', font=("宋体", 14), width=8).grid(row=1, column=0)
    reg_user_name = StringVar()
    Entry(stu_add_win, textvariable=reg_user_name, font=("宋体", 14), width=12).grid(row=1, column=1)
    # 电话
    Label(stu_add_win, text='电话', font=("宋体", 14), width=8).grid(row=2, column=0)
    reg_phone = StringVar()
    Entry(stu_add_win, textvariable=reg_phone, font=("宋体", 14), width=12).grid(row=2, column=1)
    # 省份 下拉菜单 Combobox
    Label(stu_add_win, text='省份', font=("宋体", 14), width=8).grid(row=3, column=0)
    province_text = StringVar()
    provinces = ['河北', '河南', '山西']
    province_comb = Combobox(stu_add_win, textvariable=province_text, width=13, values=provinces, state='readonly')
    # 默认选择项
    province_comb.current(0)
    province_comb.grid(row=3, column=1)

    # 性别 单选框
    rbtn_group = StringVar()
    Label(stu_add_win, text='性别', font=("宋体", 14), width=8).grid(row=4, column=0)
    Radiobutton(stu_add_win, text='男', variable=rbtn_group, value='男').grid(row=4, column=1, sticky='w')
    Radiobutton(stu_add_win, text='女', variable=rbtn_group, value='女').grid(row=4, column=1, sticky='e')

    # 爱好   复选框
    hobby_var1 = IntVar()
    hobby_var2 = IntVar()
    Label(stu_add_win, text='爱好', font=("宋体", 14), width=8).grid(row=5, column=0)
    Checkbutton(stu_add_win, text='文学', variable=hobby_var1, onvalue=1, offvalue=0).grid(row=5, column=1, sticky='w')
    # 选中为onvalue是1，不选中是offvalue是0
    Checkbutton(stu_add_win, text='体育', variable=hobby_var2, onvalue=1, offvalue=0).grid(row=5, column=1, sticky='e')

    def stu_add():
        name = reg_user_name.get()
        phone = reg_phone.get()
        sex = rbtn_group.get()
        address = province_text.get()
        hobby1 = hobby_var1.get()
        hobby2 = hobby_var2.get()
        if hobby1 == 1:
            hobby_str1 = '文学'
        else:
            hobby_str1 = '无'
        if hobby2 == 1:
            hobby_str2 = '体育'
        else:
            hobby_str2 = '无'
        stu = [name, phone, sex, address, hobby_str1, hobby_str2]
        write_excel(stu)

    #
    Button(stu_add_win, text='添加学生', command=stu_add, width=10).grid(row=6, columnspan=2, pady=20)


if __name__ == "__main__":
    # 创建主窗体
    root = Tk()
    root.title('主窗体')
    # 在屏幕左上角绘制480x640的主窗体
    root.geometry('480x640+0+0')

    # 自定义菜单
    menu_bar = Menu(root)

    # 创建listbox显示目前的学生信息
    list_value = StringVar()
    list_box = Listbox(root, listvariable=list_value, width=40)
    # tkinter 布局的三种方式：1.place(x,y) 2.gird(row=,column=) 3.pack(side=(top,bottom,left,right))
    stu_list = read_excel()
    for i in stu_list:
        list_box.insert(END, i)
    list_box.pack(side='top')

    # 创建菜单项目
    # tearoff 如果等于1，该菜单下面会有一个虚线选项（表明该菜单可以独立出来），如果设置成1则没有
    stu_menu = Menu(menu_bar, tearoff=0)
    stu_menu.add_command(label='添加', command=pop_add_stu_win)
    stu_menu.add_command(label='查询', command='')
    menu_bar.add_cascade(label='学生', menu=stu_menu)

    # create score menu
    score_menu = Menu(menu_bar, tearoff=0)
    score_menu.add_command(label='添加', command='')
    score_menu.add_command(label='查询', command='')
    menu_bar.add_cascade(label='成绩', menu=score_menu)

    root.config(menu=menu_bar)
    root.mainloop()
