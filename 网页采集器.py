# -*- coding: utf-8 -*-
# !/usr/bin/env python
# !/usr/bin/python3
# @Time    : 2021/3/14 21:02
# @Author  : buke-freedom
# @File    : 网页采集器.py
"""
# @description：

UA检测：门户网站的服务器会检测请求需求的身份标识，如果检测到发起的请求是一个浏览器则为正常的请求，
如果不是浏览器则为不正常的请求，就是基于爬虫的，那么服务器就会拒绝请求
User-Agent
UA伪装：让爬虫应对的请求伪装成一个浏览器请求

-----目前需要输入验证码，暂不可用！！！！---、
可以使用了，不能使用的原因是User-Agent写成了User_Agent。。。。
"""
import requests

if __name__ == "__main__":
    # UA伪装：将对应的User_Agent封装到一个字典中
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:86.0) Gecko/20100101 Firefox/86.0"
    }
    url = "https://www.sogou.com/web"
    # 处理url携带的参数：封装到字典中
    kw = input("input a word to search:")
    param = {
        "query": kw
    }
    # 对指定的url发起的请求对应的url是携带参数的，并且请求过程中已经处理参数
    response = requests.get(url=url, params=param, headers=headers)
    page_text = response.text
    filename = kw + ".html"

    with open(filename, "w", encoding="utf-8") as fp:
        fp.write(page_text)
    print(filename, "保存成功！！！")
