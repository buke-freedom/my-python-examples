"""
作者：buke-freedom
时间：2020-12-26
摘要：本次学习python的for和while循环的联系


"""
import random
import string

count = 0
while count < 3:
    car_nums = []   # 存储生成的20个车牌号
    for i in range(20):     # 每次生成20号车牌号
        # n1 = random.choice("ABCDEFGHJKLMNUVWXYZ")
        n1 = random.choice(string.ascii_uppercase)      # 生成京后面第一个大写字母
        n2 = "".join(random.sample(string.ascii_uppercase+string.digits, 5))    # 生成后面5位车号牌码
        c_num = f"京{n1}-{n2}"
        car_nums.append(c_num)
        print(i+1, c_num)
    choice = input("请输入你喜欢的号码：").strip()
    if choice in car_nums:
        print(f"恭喜你选择了新的车牌号码：{choice}")
        exit("Good luck")
    else:
        print("不合法选择")
    count += 1
