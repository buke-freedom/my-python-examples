# -*- coding: utf-8 -*-
# !/usr/bin/env python
# !/usr/bin/python3
# @Time    : 2021/4/8 21:00
# @Author  : buke-freedom
# @File    : label.py
"""
# @description：

"""
from PyQt5.QtWidgets import QApplication, QLabel, QMainWindow, QVBoxLayout, QDesktopWidget
from PyQt5.QtWidgets import QWidget
from PyQt5.QtGui import QPixmap, QPalette
from PyQt5.QtCore import Qt
import sys


class QlabelDemo(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        label1 = QLabel(self)
        label2 = QLabel(self)
        label3 = QLabel(self)
        label4 = QLabel(self)
        # label5 = QLabel(self)

        label1.setText("<font color=yellow>这是一个标签.</font>")
        label1.setAutoFillBackground(True)
        palette = QPalette()  # 调试板
        palette.setColor(QPalette.Window, Qt.blue)  # 设置背景色
        label1.setPalette(palette)
        label1.setAlignment(Qt.AlignCenter)  # 设置文本居中

        label2.setText("<a href='#'>欢迎使用</a>")
        label2.resize(50, 10)

        label3.setAlignment(Qt.AlignCenter)
        label3.setToolTip('这是一个标签')
        label3.setPixmap(QPixmap("./pic.png"))

        label4.setOpenExternalLinks(True)  # 当时True时打开外部链接，当是False时打开内部指定函数
        label4.setText("<a href='https://www.baidu.com'>百度一下</a>")
        label4.setAlignment(Qt.AlignRight)
        label4.setToolTip('百度一下吧QAQ')

        vlayout = QVBoxLayout()  # 创建一个垂直盒子
        vlayout.addWidget(label1)  # 在垂直盒子里添加控件
        vlayout.addWidget(label2)
        vlayout.addWidget(label3)
        vlayout.addWidget(label4)

        label2.linkHovered.connect(self.linkHovered)  # 鼠标滑过控件时候触发
        label2.linkActivated.connect(self.linkClicked)  # 鼠标点击的时候触发，不管用

        self.setLayout(vlayout)
        self.setWindowTitle('QLabel例子')

    def linkHovered(self):
        print('当鼠标滑过标签label2时触发事件')

    def linkClicked(self):
        print('当鼠标点击label2的时候触发事件')


if __name__ == "__main__":
    app = QApplication(sys.argv)
    qlabel = QlabelDemo()
    qlabel.show()
    sys.exit(app.exec())
    pass
