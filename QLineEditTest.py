# -*- coding: utf-8 -*-
# !/usr/bin/env python
# !/usr/bin/python3
# @Time    : 2021/4/24 18:11
# @Author  : buke-freedom
# @File    : QLineEditTest.py
"""
# @description：
回显模式：
1.Normal正常模式
2.NoEcho不回显模式
3.Password密码模式
4.PasswordEchoEdit先短时间显示然后变成密码模式
"""
from PyQt5.QtWidgets import QWidget, QFormLayout, QLineEdit, QApplication
import sys


class QLineEditTest(QWidget):
    def __init__(self):
        super().__init__()
        self.initUi()

    def initUi(self):
        self.setWindowTitle('QLineEdit回显模式')

        form = QFormLayout()

        normal = QLineEdit()
        noecho = QLineEdit()
        password = QLineEdit()
        passwordecho = QLineEdit()

        form.addRow("Normal", normal)
        form.addRow("NoEcho", noecho)
        form.addRow("Password", password)
        form.addRow("PasswordEchoOnEdit", passwordecho)

        # place holder text
        normal.setPlaceholderText("Normal")
        noecho.setPlaceholderText("NoEcho")
        password.setPlaceholderText("Password")
        passwordecho.setPlaceholderText("PasswordEchoOnEdit")

        normal.setEchoMode(QLineEdit.Normal)
        noecho.setEchoMode(QLineEdit.NoEcho)
        password.setEchoMode(QLineEdit.Password)
        passwordecho.setEchoMode(QLineEdit.PasswordEchoOnEdit)

        self.setLayout(form)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    qlineedit = QLineEditTest()
    qlineedit.show()
    sys.exit(app.exec())
