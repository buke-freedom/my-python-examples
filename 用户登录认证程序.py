# coding=gbk
"""
作者：buke-freedom
时间：2021-3-6
摘要：用户登录认证程序

1 创建一个字典存用户信息accounts
2 打开用户信息存储文件account.db
3 判断用户输入用户名是否存在，如果不存在提醒用户未注册，
4 如果用户名存在判断用户是否为锁定状态
5 如果未锁定，让用户输入密码，输入密码错误3次，锁定账号
6 修改锁定账号参数，将锁定账号对应字典恢复成与account.db一样格式的字符串
7 将修改后的内容写到用户文件中
"""
# 新建一个dict用来存放用户信息
accounts = {

}
# 打开用户信息数据库
f = open("account.db", "r")
# 取出用户信息并将信息处理干净
# strip() 方法用于移除字符串头尾指定的字符（默认为空格或换行符）或字符序列
# split() 通过指定分隔符对字符串进行切片
for line in f:
    line = line.strip().split(",")
    accounts[line[0]] = line  # 将用户信息写入字典
f.close()
# print(accounts)

while True:
    # 输入用户名
    username = input("Username:").strip()
    # 判断用户名是否在用户数据库中
    if username not in accounts:
        print("该用户名未注册！")
        continue
    # 判断用户名是否处于锁定状态
    elif accounts[username][2] == "锁定":
        print("该账号已被锁定，请联系管理员！")
        continue
    count = 0
    # 密码输入3次判断
    while count < 3:
        password = input("Password:").strip()
        # 判断密码是否正确
        if password == accounts[username][1]:
            print(f"恭喜'{username}'登录成功!")
            exit()
        else:
            print("密码错误")
        count += 1
    if count == 3:
        print(f"输错{count}次密码，账号{username}将被锁定！")
        # 1先修改用户锁定状态
        # 2将信息按原格式写回用户信息数据库文件中
        accounts[username][2] = "锁定"
        new_f = open("account.db", "w")
        for username, val in accounts.items():
            line = ",".join(val) + "\n"
            new_f.write(line)
        new_f.close()

        exit("bye.")
