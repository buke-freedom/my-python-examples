"""
作者：buke-freedom
时间：2020-12-24
摘要：本次学习python的列表
    创建列表：list-name = [list-element]
    追加元素：list-name.append('element')
    扩展元素：list-name.extend(another-list)
    插入元素：list-name.insert(index,'element')
    删除元素：list-name.remove('element')
    删除指定元素：del list-name[index]
    移除指定元素:list-name.pop(index)
    列表的切片：list-name[index1:index2]
        list-name[1:3],list-name[:3],list-name[3:]
    内置函数：
        count：计算元素在列表出现的次数
        index：返回元素在列表中的坐标
        reverse：将元素翻转
        sort：对列表进行排序list-name.sort()从小到大，list-name.sort(reverse=True)从大到小
"""

# 创建一个列表
list1 = [1, '测试', '学习', '努力']
# 在列表最后添加一个元素
list1.append('我也要')
# 查看列表长度
len(list1)
# 扩张列表
list2 = ['我来组成倒数第二', '我来组成尾部']
list1.extend(list2)
print(list1)
# 插入元素
list1.insert(0, '我要当第一')
print(list1)
# 获取元素
print(list1[2])
# 从列表删除元素，只需要知道元素的值，不需要知道位置
list1.remove('测试')
print(list1)
# 删除元素
del list1[1]
print(list1)
# 移除列表元素，空参数移除最后的元素
print(list1.pop())
print(list1)
print(list1.pop(1))
print(list1)
