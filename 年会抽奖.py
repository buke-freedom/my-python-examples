"""
作者：buke-freedom
时间：2020-12-26
摘要：本次学习python的for和while循环的联系

年会抽奖程序
科技有限公司有300员工，开年会抽奖，奖项如下：
一等奖3名，泰国5日游
二等奖6名，iPhone手机一部
三等奖30名，移动充电宝一个
规则：
1.共抽3次，第一次抽3等奖，第二次抽2等奖，第三次抽1等奖
2.每个员工限中奖一次，不能重复

"""
import random


IDs = []
san = []
er = []
yi = []
print("欢迎大家参加XX科技有限公司2020年年会！")
for i in range(1, 301):
    IDs.append(i)

print("---------------现在抽三等奖---------------")
san.extend(random.sample(IDs, 30))
print("获得三等奖的人员如下：")
print(san)
print("恭喜上面30名员工各获得移动充电宝一个！")
for i in range(30):
    IDs.remove(san[i])

print("---------------现在抽二等奖--------------")
er.extend(random.sample(IDs, 6))
print("获得二等奖的人员如下：")
print(er)
print("恭喜上面6名员工个获得iPhone手机一个！")
for i in range(6):
    IDs.remove(er[i])

print("--------------现在抽一等奖---------------")
yi.extend(random.sample(IDs, 3))
print("获得一等奖的人员如下：")
print(yi)
print("恭喜上面3个员工获得泰国5日游！")
for i in range(3):
    IDs.remove(yi[i])
print("未中奖名单：")
print(IDs)
