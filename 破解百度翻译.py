# -*- coding: utf-8 -*-
# !/usr/bin/env python
# !/usr/bin/python3
# @Time    : 2021/3/14 21:44
# @Author  : buke-freedom
# @File    : 破解百度翻译.py
"""
# @description：
需求：破解百度翻译
    -post请求（携带了参数）
    - 响应数据是一组json数据
"""
import requests
import json

if __name__ == "__main__":
    post_url = "https://fanyi.baidu.com/sug"
    word = input("enter a word:")
    data = {
        "kw": word
    }
    # UA伪装：将对应的User_Agent封装到一个字典中
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:86.0) Gecko/20100101 Firefox/86.0"
    }
    response = requests.post(url=post_url, data=data, headers=headers)
    # 获取数据,json()方法返回的是obj，（如果确认响应数据是json类型的，才可以使用json方法
    dic_obj = response.json()
    print(dic_obj)

    # 进行存储
    filename = word+".json"
    fp = open(filename, "w", encoding="utf-8")
    # 数据是中文的所以ensure_ascii = False
    json.dump(dic_obj, fp=fp, ensure_ascii=False)
    print("翻译完毕！！！！")
