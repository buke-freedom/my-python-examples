# -*- coding: utf-8 -*-
# !/usr/bin/env python
# !/usr/bin/python3
# @Time    : 2021/3/9 20:07
# @Author  : buke-freedom
# @File    : python_excel.py
"""
# @description：

"""
from openpyxl import Workbook

wb = Workbook()  # create an excel file

sheet = wb.active

print(sheet.title)
sheet.title = "我的excel测试"

sheet["C5"] = "ceshi"
sheet["C6"] = "zhje"

sheet.append([1, 2, 3, 4, 555, 6])

wb.save("python_excel.xlsx")
